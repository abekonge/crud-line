use std::error::Error;
use std::fs;
use std::io::prelude::*;

pub fn create(text: &str, line: &str) -> String {
    format!("{}{}\n", text, line)
}

pub fn create_unique(text: &str, pattern: &str, line: &str) -> Result<String, Box<dyn Error>> {
    if let true = text.lines().any(|line| line.contains(pattern)) {
        return Err("Pattern already exists".into());
    };
    Ok(create(text, line))
}

pub fn create_unique_exact(text: &str, line: &str) -> Result<String, Box<dyn Error>> {
    create_unique(text, &(String::from(line) + "\n"), line)
}

pub fn create_in_file(file_path: &str, line: &str) -> Result<(), Box<dyn Error>> {
    let contents = get_file_content(file_path)?;
    fs::write(file_path, create(&contents, line))?;
    Ok(())
}

pub fn create_unique_exact_in_file(file_path: &str, line: &str) -> Result<(), Box<dyn Error>> {
    create_unique_in_file(file_path, line, line)?;
    Ok(())
}

pub fn create_unique_in_file(
    file_path: &str,
    pattern: &str,
    insert_line: &str,
) -> Result<(), Box<dyn Error>> {
    // todo use get_file_content and create_unique and put into file
    let lookup_file = fs::OpenOptions::new()
        .read(true)
        .create(false)
        .append(true)
        .open(file_path);
    let mut lookup_file = match lookup_file {
        Ok(file) => file,
        Err(e) => {
            return Err(format!("There was a problem with the file {}: {}", file_path, e).into())
        }
    };
    // check if line exists, if  yes fail
    let mut contents = String::new();
    lookup_file.read_to_string(&mut contents)?;
    if let true = contents.lines().any(|line| line.contains(pattern)) {
        return Err("Pattern already exists".into());
    };
    // insert line
    writeln!(lookup_file, "{}", insert_line)?;
    Ok(())
}

pub fn read_line(text: &str, pattern: &str) -> Result<String, Box<dyn Error>> {
    match text.lines().find(|line| line.contains(pattern)) {
        Some(line) => Ok(String::from(line)),
        None => Err("Cannot find pattern".into()),
    }
}

// read only lines exactly matching
pub fn read_exact(text: &str, line: &str) -> Result<String, Box<dyn Error>> {
    // let full_line = String::from(line) + "\n";
    match text.lines().find(|l| *l == line) {
        Some(line) => Ok(String::from(line)),
        None => Err("Cannot find line".into()),
    }
}

pub fn read_line_from_file(file_path: &str, pattern: &str) -> Result<String, Box<dyn Error>> {
    let contents = get_file_content(file_path)?;
    return read_line(&contents, pattern);
}

pub fn read_exact_from_file(file_path: &str, line: &str) -> Result<String, Box<dyn Error>> {
    let contents = get_file_content(file_path)?;
    return read_exact(&contents, line);
}

pub fn update_line_in_file(
    file_path: &str,
    pattern: &str,
    update: &str,
) -> Result<(), Box<dyn Error>> {
    let contents = get_file_content(file_path)?;

    let updated = update_line(contents, pattern, update)?;

    fs::write(file_path, updated)?;
    Ok(())
}

pub fn update_line(text: String, pattern: &str, update: &str) -> Result<String, Box<dyn Error>> {
    if !text.lines().any(|l| l.contains(pattern)) {
        return Err(format!("The pattern: {}, does not exist. Update failed", pattern).into());
    }
    let updated: String = text
        .lines()
        .map(|line| {
            if line.contains(pattern) {
                String::from(update) + "\n"
            } else {
                String::from(line) + "\n"
            }
        })
        .collect();
    Ok(updated)
}

pub fn delete_line(text: String, pattern: &str) -> Result<String, Box<dyn Error>> {
    if !text.lines().any(|l| l.contains(pattern)) {
        return Err(format!("The pattern: {}, does not exist. Delete failed", pattern).into());
    }
    let remaining: String = text.lines().fold(String::new(), |lines, line| {
        if line.contains(pattern) {
            lines
        } else {
            format!("{}{}\n", lines, line)
        }
    });
    Ok(remaining)
}

pub fn delete_line_in_file(file_path: &str, pattern: &str) -> Result<(), Box<dyn Error>> {
    let contents = get_file_content(file_path)?;
    let remaining = delete_line(contents, pattern)?;

    fs::write(file_path, remaining)?;
    Ok(())
}

fn get_file_content(file_path: &str) -> Result<String, Box<dyn Error>> {
    let lookup_file = fs::OpenOptions::new().read(true).open(file_path);
    let mut lookup_file = match lookup_file {
        Ok(file) => file,
        Err(e) => {
            return Err(format!("There was a problem with the file {}: {}", file_path, e).into())
        }
    };

    let mut contents = String::new();
    lookup_file.read_to_string(&mut contents)?;
    Ok(contents)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn reads_line() {
        let text = "\
first line
second line
";
        let expected = "first line";
        let result = read_line(text, "first").unwrap();
        assert_eq!(expected, result);
    }

    #[test]
    fn reads_full_line() {
        let text = "\
first line
second line
";
        let expected = "first line";
        let result = read_exact(text, "first line").unwrap();
        assert_eq!(expected, result);
    }

    #[test]
    fn creates_line() {
        let text = "\
first line
second line
";
        let text = &create_unique(text, "third", "third line").unwrap();
        let expected = "\
first line
second line
third line
";
        assert_eq!(text, expected);
    }

    #[test]
    #[should_panic]
    fn doesnt_create() {
        let text = "\
        first line";
        let _text = &create_unique(text, "first", "third line").unwrap();
    }

    #[test]
    fn updates_line() {
        let text = "\
first line
second line
third line
";
        let updated = update_line(String::from(text), "first", "updated line").unwrap();
        let expected = "\
updated line
second line
third line
";
        assert_eq!(updated, expected);
    }

    #[test]
    fn deletes_line() {
        let text = "\
first line
second line
third line
";
        let remaining = delete_line(String::from(text), "first").unwrap();
        let expected = "\
second line
third line
";
        assert_eq!(remaining, expected);
    }
    #[test]
    #[should_panic]
    fn doesnt_delete() {
        let text = "\
        first line";
        let _text = &delete_line(String::from(text), "uggi").unwrap();
    }

    #[test]
    fn creates_unique_line_in_file() {
        // prepare
        fs::File::create("test1.txt").unwrap();

        let new_line = "new line";

        // test
        create_unique_in_file("test1.txt", "nope", new_line).unwrap();
        let read = read_line_from_file("test1.txt", "new").unwrap();
        assert_eq!(new_line, read);

        // clean
        fs::remove_file("test1.txt").unwrap();
    }

    #[test]
    #[should_panic]
    fn doesnt_create_in_file() {
        // prepare
        let _ = fs::File::create("test2.txt");

        let _ = fs::write("test2.txt", "existing pattern");

        // setup
        let err = create_unique_in_file("test2.txt", "existing pattern", "new_line");

        // needs to remove file before expected panic
        let _ = fs::remove_file("test2.txt");

        //  test, try to unwrap error, should panic
        err.unwrap();
    }
    #[test]
    fn updates_line_in_file() {
        // prepare
        fs::File::create("test3.txt").unwrap();
        create_unique_in_file("test3.txt", "_", "this is the original line").unwrap();

        // test
        update_line_in_file(
            "test3.txt",
            "this is the original line",
            "this is the updated line",
        )
        .unwrap();
        let updated_line = read_line_from_file("test3.txt", "updated").unwrap();
        assert_eq!("this is the updated line", updated_line);

        // clean
        fs::remove_file("test3.txt").unwrap();
    }

    #[test]
    #[should_panic]
    fn deletes_line_in_file() {
        // prepare
        fs::File::create("test4.txt").unwrap();
        let _ = create_unique_in_file("test4.txt", "_", "this is the original line");

        // test
        let _ = delete_line_in_file("test4.txt", "this is the original line");
        // should panic
        let err = read_line_from_file("test4.txt", "this is the original line");

        // needs to remove file before expected panic
        let _ = fs::remove_file("test4.txt");

        //  test, try to unwrap error, should panic
        err.unwrap();
    }

    #[test]
    fn creates_line_in_file() {
        // prepare
        fs::File::create("test5.txt").unwrap();

        // test
        create_in_file("test5.txt", "a line").unwrap();
        let result = read_line_from_file("test5.txt", "a line").unwrap();
        assert_eq!("a line", result);

        // clean
        fs::remove_file("test5.txt").unwrap();
    }

    #[test]
    #[should_panic]
    fn creates_unique_exact_line_in_file() {
        // prepare
        fs::File::create("test6.txt").unwrap();
        let _ = create_in_file("test6.txt", "a unique line");

        // test
        let result = create_unique_exact_in_file("test6.txt", "a unique line");
        // needs to remove file before expected panic
        fs::remove_file("test6.txt").unwrap();
        result.unwrap();
    }

    #[test]
    fn reads_unique_in_file() {
        // prepare
        let file_path = "test7.txt";
        fs::File::create(file_path).unwrap();
        let unique_line = "unique line";
        create_in_file(file_path, "new line").unwrap();
        create_unique_exact_in_file(file_path, unique_line).unwrap();

        // test
        let read = read_exact_from_file(file_path, unique_line).unwrap();
        assert_eq!(unique_line, read);

        // clean
        fs::remove_file(file_path).unwrap();
    }

    #[test]
    #[should_panic]
    fn doesnt_read_unique_in_file() {
        // prepare
        let file_path = "test8.txt";
        fs::File::create(file_path).unwrap();
        let full_line = "line with more to it";
        let partial_line = "line";
        create_in_file(file_path, full_line).unwrap();

        // test
        let result = read_exact_from_file(file_path, partial_line);

        // needs to remove file before expected panic
        let _ = fs::remove_file(file_path);

        //  test, try to unwrap expected error, should panic
        result.unwrap();
    }
}
