# crud-line

A crate for performing CRUD-operations with lines in strings or files

# TODO

- [x] make update string
- [x] make update file
- [x] make delete from file
- [x] refactor crud operations out from file context, to work on strings,
  - [x] update in string
  - [x] read from string
  - [x] delete from string
  - [x] insert in string
  - [x] test read from string
- [ ] New feature: returning reverse operation from succesful op? as Ok(reverse_op)
  - ## Maybe have it as a struct with a result code, a reverse method, a retry method?
  - return: Result<Box<dyn Fn() -> ()>, Box<dyn Error>>
  - like this:
    - let reverse_op = Box::new(||{delete_from_file(file_path, pattern); return ()});
    - Ok(reverse_op)
- [ ] use string operations in file operations (if feasible)
- [x] rm custom error messages.
- [x] rename to CRUD verbs
- [x] write tests with test files
- [x] consider having additional functions for create and create_unique, and update and update_unique and update_first, delete and delete_unique and delete_first
- [ ] Extract write String to file (overwrite)

CREATE

- [x] create (text:String, new_line: &str)
- [x] create_in_file
- [x] create_unique (text: String, pattern: &str, new_line: &str)
- [x] create_create_unique_in_file
- [x] create_unique_exact (text: String, new_line: &str)
- [x] create_unique_exact_in_file

READ

- [x] read (text: String, pattern: &str)
- [x] read_from_file
- [x] read_exact (text: String, line: &str)
- [x] read_exact_from_file

UPDATE

- [x] update (text: String, pattern: &str, update_line: &str)
- [x] update_in_file
- [ ] update_exact (text: String, old_line: &str, update_line: &str)
- [ ] update_exact_in \_file

DELETE

- [x] delete (text: String, pattern: str)
- [x] delete_in_file
- [ ] delete_exact (text: String, line: &str)
- [ ] delete_exact_in_file
